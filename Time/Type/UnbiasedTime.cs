﻿using System;
using TMI.TimeManagement;

namespace TMI.ProjectFramework.TimeManagement {

    public class UnbiasedTime : ITime {

        public DateTime now {
            get {
                return global::UnbiasedTime.Instance.Now();
            }
        }

    }

}